# PDBget
[![Crates.io](https://img.shields.io/crates/v/GetPDB?logoColor=orange)](https://crates.io/crates/GetPDB)
[![license](http://img.shields.io/badge/license-Apache%20v2-green.svg)](https://gitlab.com/philippe_noel/pdbget/blob/master/LICENSE)  

**CLI program to download files from rcsb.org or PDBe**

## Installation

### Using Cargo
```bash
cargo install GetPDB
pdbget -v
```

### From sources
```bash
git clone https://gitlab.com/philippe_noel/pdbget.git
cd pdbget
cargo install --path
pdbget --help
```

## Usage
```bash
Get Protein files 1.0.1
Philippe Noel
Download Protein files on rcsb.org or pdbe

USAGE:
    pdbget [OPTIONS] <PDBs>...

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -o <Output>         Output folder where to store files [default: ./]
    -s <Server>         Server name where to download pdb files. 'rcsb', 'pdbe'.
                        Format for rcsb: 'fasta', 'pdb', 'pdbgz', 'cif', 'cifgz', 'xmlgz'.
                        Format for pdbe: 'fasta', 'pdb', 'pdbgz', 'cif', 'xml'.
                         [default: rcsb]
    -t <UriType>        File type to download. 'pdb', 'pdbgz', 'cif', 'cifgz', 'fasta', 'xml', 'xmlgz' [default: PDB]

ARGS:
    <PDBs>...    PDB identifiers
```
With the **-s option**, you can specify the server where download files. Possible server are:
- rcsb : [RCSB.org](https://www.rcsb.org/)
- pdbe : [PDBe](https://www.ebi.ac.uk/pdbe/)
Note that all formats are not available for all server. Check the table.

| **Format** | **RCSB**               | **PDBe**               |
|:------:|:------------------:|:------------------:|
| **FASTA**  | ✔ | ✔ |
| **PDB**    | ✔ | ✔ |
| **PDBGZ**  | ✔ | ✔ |
| **CIF**    | ✔ | ✔ |
| **CIFGZ**  | ✔ | ❌|
| **XML**    | ❌| ✔ |
| **XMLGZ**  | ✔ | ❌|

With the **-t option**, you can specify the output file format. Possible format are:
- pdb : [PDB format](http://pdb101.rcsb.org/learn/guide-to-understanding-pdb-data/introduction)
- pdbgz : GZ compression of a PDB file
- cif : [CIF format](https://www.iucr.org/resources/cif)
- cifgz : GZ compression of a CIF file
- fasta : FASTA sequence(s) of the protein
- xml : GZ compression of an XML file (uncompress XML are not provide on rcsb.org)
