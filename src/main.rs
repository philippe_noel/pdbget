use clap::{App, Arg};
use pdbget;
use pdbget::{input_output, request};
use std::io;

fn main() -> Result<(), io::Error> {
    let matches = App::new("Get Protein files")
        .version("1.0.1")
        .author("Philippe Noel")
        .about("Download Protein files on rcsb.org")
        .arg(
            Arg::with_name("PDBs")
                .help("PDB identifiers")
                .required(true)
                .min_values(1),
        )
        .arg(
            Arg::with_name("Output")
                .help("Output folder where to store files")
                .short("o")
                .default_value("./"),
        )
        .arg(
            Arg::with_name("UriType")
                .help("File type to download. 'pdb', 'pdbgz', 'cif', 'cifgz', 'fasta', 'xml', 'xmlgz'")
                .short("t")
                .default_value("PDB"),
        )
        .arg(
            Arg::with_name("Server")
                .help(
                    "Server name where to download pdb files. 'rcsb', 'pdbe'.
Format for rcsb: 'fasta', 'pdb', 'pdbgz', 'cif', 'cifgz', 'xmlgz'.
Format for pdbe: 'fasta', 'pdb', 'pdbgz', 'cif', 'xml'.\n",
                )
                .short("s")
                .default_value("rcsb"),
        )
        .get_matches();

    let pdb_identifiers: Vec<_> = matches.values_of("PDBs").unwrap().collect(); // Call unwrap because PDBs is required in clap

    // Use unwrap unpack argument from 'matches' because argument have default enable
    let output_folder = input_output::check_output(matches.value_of("Output").unwrap())?;

    let server = pdbget::uri_types::Server::get_server(matches.value_of("Server").unwrap())?;
    let uri_type = pdbget::uri_types::UriType::get_type(matches.value_of("UriType").unwrap())?; // Use unwrap because default is set
    let extension = uri_type.get_extension();

    for pdb in pdb_identifiers {
        let mut writer: Vec<u8> = vec![]; // Create a buffer writer which will contain each bytes of the GET request
        match request::get(&uri_type.generate_uri(pdb, &server)?, &mut writer) {
            Ok(_) => (),
            Err(e) => {
                eprintln!("[Warning] problem with '{}' id\n{}", pdb, e);
                continue; // Got to the next iteration
            }
        };
        let mut output_filename = output_folder.clone();
        output_filename.push(format!("{}{}", pdb, extension));
        input_output::write_file(output_filename, writer)?;
    }

    Ok(())
}
