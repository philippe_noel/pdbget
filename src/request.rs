use reqwest;
use std::io;

/// Transform reqwest::Error to std::io::Error
pub fn handled_reqwest_errors(uri: &str, e: reqwest::Error) -> io::Error {
    io::Error::new(
        io::ErrorKind::ConnectionAborted,
        format!("[Warning] when connecting to {}\n{}", uri, e.to_string()),
    )
}

/// Function to performed a GET request on the url and write the return
/// in the writer buffer
pub fn get(uri: &str, writer: &mut Vec<u8>) -> Result<(), io::Error> {
    let mut result = match reqwest::Client::new().get(uri).send() {
        Ok(res) => res,
        Err(e) => return Err(handled_reqwest_errors(uri, e)),
    };
    if result.status() != 200 {
        return Err(io::Error::new(
            io::ErrorKind::ConnectionAborted,
            format!(
                "[Warning] when connecting to {}\nResponse Statut : {}",
                uri,
                result.status()
            ),
        ));
    }
    match result.copy_to(writer) {
        Ok(_) => Ok(()),
        Err(e) => Err(handled_reqwest_errors(uri, e)),
    }
}
