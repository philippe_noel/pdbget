use std::io;

#[derive(Debug)]
pub enum UriType {
    Fasta,
    Pdb,
    PdbGz,
    Cif,
    CifGz,
    Xml,
    XmlGz,
}

#[derive(Debug)]
pub enum Server {
    PDBE,
    RCSB,
}

impl Server {
    pub fn get_server(s: &str) -> Result<Server, io::Error> {
        match s.to_uppercase().as_ref() {
            "RCSB" => Ok(Server::RCSB),
            "PDBE" => Ok(Server::PDBE),
            _ => Err(io::Error::new(
                io::ErrorKind::InvalidInput,
                format!(
                    "[Error] '{}' is not an accepted server. Use -h to see available severs",
                    s
                ),
            )),
        }
    }
}

impl UriType {
    pub fn get_type(t: &str) -> Result<UriType, io::Error> {
        let t = String::from(t);
        match t.to_uppercase().as_ref() {
            "FASTA" => Ok(UriType::Fasta),
            "PDB" => Ok(UriType::Pdb),
            "PDBGZ" => Ok(UriType::PdbGz),
            "CIF" => Ok(UriType::Cif),
            "CIFGZ" => Ok(UriType::CifGz),
            "XMLGZ" => Ok(UriType::XmlGz),
            "XML" => Ok(UriType::Xml),
            _ => Err(io::Error::new(
                io::ErrorKind::InvalidInput,
                format!(
                    "[Error] '{}' is not an accepted format. Use -h to see available types",
                    t
                ),
            )),
        }
    }

    pub fn generate_uri(&self, id: &str, server: &Server) -> Result<String, io::Error> {
        match server {
            Server::RCSB =>
                match self {
                    UriType::Fasta => Ok(format!("https://www.rcsb.org/pdb/download/downloadFastaFiles.do?structureIdList={}&compressionType=uncompressed", id)),
                    UriType::Pdb => Ok(format!("https://files.rcsb.org/download/{}.pdb", id)),
                    UriType::PdbGz => Ok(format!("https://files.rcsb.org/download/{}.pdb.gz", id)),
                    UriType::Cif => Ok(format!("https://files.rcsb.org/download/{}.cif", id)),
                    UriType::CifGz => Ok(format!("https://files.rcsb.org/download/{}.cif.gz", id)),
                    UriType::XmlGz => Ok(format!("https://files.rcsb.org/download/{}.xml.gz", id)),
                    _ => Err(
                        io::Error::new(
                            io::ErrorKind::InvalidData,
                            format!(
                                "[Error] '{:?}' format is not supported on {:?} server",
                                self,
                                server
                            )
                        )
                    ),
                },
            Server::PDBE =>
                match self {
                    UriType::Fasta => Ok(format!("http://www.ebi.ac.uk/pdbe/entry/pdb/{}/fasta", id)),
                    UriType::Pdb => Ok(format!("http://www.ebi.ac.uk/pdbe/entry-files/download/pdb{}.ent", id)),
                    UriType::PdbGz => Ok(format!("http://ftp.ebi.ac.uk/pub/databases/rcsb/pdb-remediated/data/structures/divided/pdb/ci/pdb{}.ent.gz", id)),
                    UriType::Cif => Ok(format!("http://www.ebi.ac.uk/pdbe/entry-files/download/{}_updated.cif", id)),
                    UriType::Xml => Ok(format!("https://www.ebi.ac.uk/pdbe/entry-files/download/{}_validation.xml", id)),
                    _ => Err(
                        io::Error::new(
                            io::ErrorKind::InvalidData,
                            format!(
                                "[Error] '{:?}' format is not supported on {:?} server",
                                self,
                                server
                            )
                        )
                    ),
                },
        }
    }

    pub fn get_extension(&self) -> &str {
        match self {
            UriType::Fasta => ".fasta",
            UriType::Pdb => ".pdb",
            UriType::Cif => ".cif",
            UriType::PdbGz => ".pdb.gz",
            UriType::CifGz => ".cif.gz",
            UriType::XmlGz => ".xml.gz",
            UriType::Xml => ".xml",
        }
    }
}
