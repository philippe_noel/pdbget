use std::{fs, io, io::Write, path::PathBuf};

pub fn write_file(filename: PathBuf, writer: Vec<u8>) -> Result<(), io::Error> {
    let mut output_pdb = fs::File::create(filename)?;
    let _ = output_pdb.write(&writer)?;

    Ok(())
}

/// Check the output directory pass in argument of the program
/// Check if the folder exist, if not create it
/// Return an error if it's not possible, or in case of wrong permissions access
pub fn check_output(output: &str) -> Result<PathBuf, io::Error> {
    let path = PathBuf::from(output);
    if path.is_dir() {
        return Ok(path);
    }
    fs::create_dir(&path)?;
    Ok(path)
}
